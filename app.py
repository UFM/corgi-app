from flask import Flask, render_template
import random

app = Flask(__name__)

# list of cat images
images = [
    "https://media.giphy.com/media/RuP38JSfj14je/giphy.gif",
    "https://media.giphy.com/media/vGo2sgzeC8r60/giphy.gif",
    "https://media.giphy.com/media/WEiKBTaESHHhK/giphy.gif",
    "https://media.giphy.com/media/ZjUjG4xgRiSOc/giphy.gif",
    "https://media.giphy.com/media/xrybHE4mp40dG/giphy.gif",
    "https://media.giphy.com/media/5EJHDSPpFhbG0/giphy.gif",
    "https://media.giphy.com/media/gFbeY4qiYrHOg/giphy.gif",
    "https://media.giphy.com/media/VFDeGtRSHswfe/giphy.gif",
    "https://media.giphy.com/media/cIQkxw4G65TG/giphy.gif",
    "https://media.giphy.com/media/VOCylsEUWBkOs/giphy.gif",
    "https://media.giphy.com/media/zdffjd0BKYcAU/giphy.gif"
]

@app.route('/')
def index():
    url = random.choice(images)
    return render_template('index.html', url=url)

if __name__ == "__main__":
    app.run(host="0.0.0.0")

